function addChar(char) {
    var input = document.getElementById('input');
    input.value += char;
}

function clearInput() {
    var input = document.getElementById('input');
    input.value = '';
}

function calculate() {
    var input = document.getElementById('input');
    var result = eval(input.value);
    input.value = result;
}

